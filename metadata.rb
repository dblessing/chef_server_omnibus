name             'chef_server_omnibus'
maintainer       'Drew Blessing'
maintainer_email 'cookbooks@blessing.io'
license          'Apache 2.0'
description      'Installs/Configures Chef Server'
long_description 'A simple cookbook for installing and configuring Chef Server, ' \
                 'including Chef Server Enterprise add-ons.'
version          '0.3.0'

depends 'chef-server-ingredient', '~> 0.3'
depends 'poise', '~> 2.0'

supports 'centos', '>= 6.5'
supports 'debian', '>= 7.1'
supports 'ubuntu', '>= 12.04'
