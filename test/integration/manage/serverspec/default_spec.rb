require 'spec_helper'

describe 'chef_server_omnibus::default' do
  describe command('/opt/opscode/bin/chef-server-ctl status').stdout do
    it { is_expected.to match(/run/) }
    it { is_expected.not_to match(/down/) }
  end

  describe command('/opt/opscode/bin/chef-server-ctl user-create users1 User One user1@example.com 12345678').stdout do
    it { is_expected.to match(/-----BEGIN RSA PRIVATE KEY-----/) }
  end
end
