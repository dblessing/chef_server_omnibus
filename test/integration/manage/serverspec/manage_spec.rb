require 'spec_helper'

describe 'chef_server_omnibus::manage' do
  describe command('/opt/opscode-manage/bin/opscode-manage-ctl status').stdout do
    it { is_expected.to match(/run/) }
    it { is_expected.not_to match(/down/) }
  end

  describe command('wget --no-check-certificate -qO- https://localhost').stdout do
    it { is_expected.to match(/Sign In/) }
  end
end
