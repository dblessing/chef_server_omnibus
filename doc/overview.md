A simple cookbook for installing and configuring Chef Server, including
Chef Server Enterprise add-ons.

# Getting Started

## Chef Server Core

Include the default recipe to install `chef-server-core` and render the `/etc/opscode/chef-server.rb`
configuration file. By default, `api_fqdn` is set to `node['fqdn']` and `topology`
is set to `standalone`. No attributes are required.

Configuration directives in the
[chef-server.rb Settings](http://docs.chef.io/server/config_rb_server.html) documentation under
[Recommended Settings](http://docs.chef.io/server/config_rb_server.html#recommended-settings)
each have specific node attributes. For example, to set `api_fqdn` use
`node.default['chef_server_omnibus']['api_fqdn'] = 'chef.example.com'`.

For the additional configuration keys there is a configuration
hash in this cookbook `node['chef_server_omnibus']['config']`. For example, to set `bookshelf['vip']` use
`node.default['chef_server_omnibus']['config']['bookshelf']['vip'] = '127.0.0.1'`.
Similarly, to set `nginx['ssl_ciphers']` use
`node.default['chef_server_omnibus']['config']['nginx']['ssl_ciphers'] = 'TLSv1.2'`.

See [chef-server.rb Settings](http://docs.chef.io/server/config_rb_server.html)
documentation for more information on configuration options.

## Enterprise Add-Ons

Chef Manage, Reporting, and Push Jobs are enterprise add-ons. To install the add-ons. Put the
corresponding recipe in your Chef Server's runlist. None of the three add-ons here require any
node attributes.

* `chef_server_omnibus::manage`
* `chef_server_omnibus::push_jobs`
* `chef_server_omnibus::reporting`

### Note for `chef_server_omnibus::reporting`

Reporting add-on requires the Manage interface. Therefore, the `chef_server_omnibus::reporting` recipe includes
`chef_server_omnibus::manage`.

### Note for `chef_server_omnibus::push_jobs`

This recipe installs push jobs server. All nodes will also need the push jobs client. Use the
[push-jobs](https://supermarket.chef.io/cookbooks/push-jobs) cookbook from the supermarket to manage client
installation.

## Analytics Server

Chef Analytics is also an Enterprise add-on but it runs as a separate server (technically, it can be combined with
Chef Server). By contrast, all of the add-ons above *must* run on the same server as Chef Server core. To install Chef
Analytics, include `chef_server_omnibus::analytics` recipe. By default, `analytics_fqdn` and `topology`
are set. Set other Analytics configuration options using `node.default['chef_server_omnibus']['analytics']['config']` hash
in exactly the same way as described above for the `chef-server.rb` file.

To connect Analytics to the Chef Server some additional configuration is required on the Chef Server node. Add the
following node attributes on the Chef Server, changing values as appropriate:

```ruby
node.default['chef_server_omnibus']['config']['oc_id']['applications'] = {
  'analytics' => {
    'redirect_uri' => 'https://<analytics_fqdn>/'
  }
}

node.default['chef_server_omnibus']['config']['rabbitmq']['vip'] = '<ip_of_chef_server>'
node.default['chef_server_omnibus']['config']['rabbitmq']['node_ip_address'] = '0.0.0.0'
```

Note: If the above configuration changes on the Chef Server the contents of `/etc/opscode-analytics` will
likely need copied to the Analytics server and then run `opscode-analytics-ctl reconfigure`.

## Replication/Chef Sync

From Chef documentation: "Chef replication provides a way to asynchronously distribute cookbook, environment,
role, and data bag data from a single, primary Chef server to one (or more) replicas of that Chef server."

From experience, Chef replication requires advanced configuration and great understanding of how Chef server works.
Make sure to explore replication and its configuration before using this cookbook to configure it. Also note that since
Chef replication requires files/directories to be copied from master to replicas this cookbook does not
facilitate that. Files will have to be copied manually or use a wrapper cookbook with `chef-provisioning` to move
files around.

To install the `chef-sync` add-on, include the `chef_server_omnibus::sync` recipe on any master or replica server.
For the best up to date information on Chef Sync/Replication see
[Chef Replication](https://docs.chef.io/server_replication.html) documentation. At least you will need to configure the
following node attributes.

* `node['chef_server_omnibus']['sync']['role']`
* `node['chef_server_omnibus']['sync']['master']` (if role is `:replica` or `:master_and_replica`)
* `node['chef_server_omnibus']['sync']['organizations']` (if role is `:replica` or `:master_and_replica`)

For the additional configuration keys there is a configuration hash in this cookbook
`node['chef_server_omnibus']['sync']['config']`. For example, to set
`ec_sync_client['dir'] = '/var/opt/chef-sync/ec_sync_client'` use
`node.default['chef_server_omnibus']['sync']['config']['ec_sync_client']['dir'] = '/var/opt/chef-sync/ec_sync_client'`.
