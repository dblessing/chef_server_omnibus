## Report issues

[Issue Tracker](https://gitlab.com/dblessing/chef_server_omnibus/issues)

If the documentation here leaves you stuck or you think you've encountered a bug, please report an issue
using the link above. The intention is for documentation to be clear enough to help anyone get up and running.
If it is not clear enough that *is* a bug - please report it. 

## Things this cookbook *doesn't* do:

* Manage a firewall
* Manage secrets (database passwords, SSL keys/certs, etc)
* Orchestrate Chef Server cluster provisioning.

### Why not?

This is a library/application cookbook. It's sole purpose is installation
and configuration of Chef Server. The goal is to be forward
compatible with future versions of Chef Server and avoid assumptions about how
users like to use Chef. Therefore, it does not validate whether configuration
hash values are valid, it does not require any data bags,
manage secrets, install SSL certificates, or anything else of that nature.
This leaves users free to wrap the cookbook and add those bits that work for
their environment.

For an example of how to orchestrate Chef Server cluster provisioning, see
Chef's [Chef Server Cluster](https://github.com/opscode-cookbooks/chef-server-cluster)
cookbook.

