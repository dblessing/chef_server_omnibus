#
# Cookbook Name:: chef_server_omnibus
# Spec:: analytics
#
# Copyright (c) 2015 Drew Blessing, All Rights Reserved.

require 'spec_helper'

describe 'chef_server_omnibus::analytics' do
  context 'with default attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new.converge(described_recipe)
    end
    subject { chef_run }

    [
      'analytics_fqdn \'chefspec.local\'',
      'topology \'standalone\''
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/opscode-analytics/opscode-analytics.rb')
            .with_content(content)
        )
      end
    end
  end

  context 'with config attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.set['chef_server_omnibus']['analytics']['config']['actions_consumer']['hipchat_room'] = 'my_room'
        node.set['chef_server_omnibus']['analytics']['config']['nginx']['ssl_protocols'] = 'TLSv1.2'
        node.set['chef_server_omnibus']['analytics']['config']['actions']['manage_endpoint'] = 'http://chef-manage'
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'actions_consumer[\'hipchat_room\'] = \'my_room\'',
      'nginx[\'ssl_protocols\'] = \'TLSv1.2\'',
      'actions[\'manage_endpoint\'] = \'http://chef-manage\''
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/opscode-analytics/opscode-analytics.rb')
            .with_content(content)
        )
      end
    end
  end
end
