#
# Cookbook Name:: chef_server_omnibus
# Spec:: sync
#
# Copyright (c) 2015 Drew Blessing, All Rights Reserved.

require 'spec_helper'

describe 'chef_server_omnibus::sync' do
  context 'with minimal attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.set['chef_server_omnibus']['sync']['master'] = 'chef-master.example.com'
        node.set['chef_server_omnibus']['sync']['organizations'] = [
          {
            :source      => 'my_source_org',
            :destination => 'my_dest_org'
          }
        ]
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'role :replica',
      'master \'chef-master.example.com\'',
      'organizations [{:source=>"my_source_org", :destination=>"my_dest_org"}]'
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/chef-sync/chef-sync.rb')
            .with_content(content)
        )
      end
    end
  end

  context 'with multiple organizations' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.set['chef_server_omnibus']['sync']['master'] = 'chef-master.example.com'
        node.set['chef_server_omnibus']['sync']['organizations'] = [
          {
            :source      => 'my_source_org',
            :destination => 'my_dest_org'
          },
          {
            :source      => 'my_source_org2',
            :destination => 'my_dest_org2'
          }
        ]
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'role :replica',
      'master \'chef-master.example.com\'',
      'organizations [{:source=>"my_source_org", :destination=>"my_dest_org"}, {:source=>"my_source_org2", :destination=>"my_dest_org2"}]'
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/chef-sync/chef-sync.rb')
            .with_content(content)
        )
      end
    end
  end

  context 'with non-default attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.set['chef_server_omnibus']['sync']['bootstrap'] = true
        node.set['chef_server_omnibus']['sync']['chef_base_path'] = '/opt/my_chef'
        node.set['chef_server_omnibus']['sync']['install_path'] = '/var/opt/sync'
        node.set['chef_server_omnibus']['sync']['name'] = 'foo'
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'bootstrap true',
      'chef_base_path \'/opt/my_chef\'',
      'install_path \'/var/opt/sync\'',
      'name \'foo\''
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/chef-sync/chef-sync.rb')
            .with_content(content)
        )
      end
    end
  end

  context 'with config attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.set['chef_server_omnibus']['sync']['config']['ec_sync_client']['dir'] = '/var/opt/sync/ec_sync_client'
        node.set['chef_server_omnibus']['sync']['config']['ec_sync_server']['ha'] = true
        node.set['chef_server_omnibus']['sync']['config']['ec_sync_client']['log_rotation'] = {
          'file_maxbytes' => 204_857_600,
          'num_to_keep'   => 5
        }
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'ec_sync_client[\'dir\'] = \'/var/opt/sync/ec_sync_client\'',
      'ec_sync_server[\'ha\'] = true',
      'ec_sync_client[\'log_rotation\'] = {"file_maxbytes"=>204857600, "num_to_keep"=>5}'
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/chef-sync/chef-sync.rb')
            .with_content(content)
        )
      end
    end
  end
end
