#
# Cookbook Name:: chef_server_omnibus
# Spec:: default
#
# Copyright (c) 2015 Drew Blessing, All Rights Reserved.

require 'spec_helper'

describe 'chef_server_omnibus::default' do
  context 'with default attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new.converge(described_recipe)
    end
    subject { chef_run }

    [
      'api_fqdn \'chefspec.local\'',
      'topology \'standalone\''
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/opscode/chef-server.rb')
            .with_content(content)
        )
      end
    end
  end

  context 'with topology, bootstrap, ip_version and notification email attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.set['chef_server_omnibus']['topology'] = 'tiered'
        node.set['chef_server_omnibus']['bootstrap'] = false
        node.set['chef_server_omnibus']['ip_version'] = 'ipv6'
        node.set['chef_server_omnibus']['notification_email'] = 'admin@example.com'
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'topology \'tiered\'',
      'bootstrap false',
      'ip_version \'ipv6\'',
      'notification_email \'admin@example.com\''
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/opscode/chef-server.rb')
            .with_content(content)
        )
      end
    end
  end

  context 'with config attributes' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.set['chef_server_omnibus']['config']['bookshelf']['vip'] = '127.0.0.1'
        node.set['chef_server_omnibus']['config']['nginx']['ssl_protocols'] = 'TLSv1.2'
        node.set['chef_server_omnibus']['config']['rabbitmq']['vip'] = nil
        node.set['chef_server_omnibus']['config']['postgresql']['foo'] = {
          'bar' => 'baz'
        }
      end.converge(described_recipe)
    end
    subject { chef_run }

    [
      'bookshelf[\'vip\'] = \'127.0.0.1\'',
      'nginx[\'ssl_protocols\'] = \'TLSv1.2\'',
      'rabbitmq[\'vip\'] = nil',
      'postgresql[\'foo\'] = {"bar"=>"baz"}'
    ].each do |content|
      it do
        is_expected.to(
          render_file('/etc/opscode/chef-server.rb')
            .with_content(content)
        )
      end
    end
  end
end
